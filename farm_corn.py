from auth import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from time import sleep
from random import randint

browser = webdriver.Firefox()
browser.implicitly_wait(1)

browser.get("https://farmrpg.com/#!/login.php")

sleep(2)

browser.find_element(
    By.CSS_SELECTOR, value="input[name='username']"
    ).send_keys(USERNAME)
browser.find_element(
    By.CSS_SELECTOR, value="input[name='password']"
    ).send_keys(PASSWORD)

sleep(1)

browser.find_element(
    By.CSS_SELECTOR, value="input[value='Login']"
    ).click()

sleep(randint(2,6))

sementeira = 0
# Vou à quinta
browser.find_element(
    By.XPATH, value="//a[contains(@href,'xfarm')]"
    ).click()
sleep(randint(4,7))

while True:
    #sementeira = 0
    #Apanho o que estiver plantado
    browser.find_element(
        By.XPATH, value="//a[contains(@class,'harvestall')]"
        ).click()
    sleep(randint(2,4))

    #seleciono o que quero semear
    #Carrots 20
    #Corn 64
    #Hops 47
    #Leek 51
    legume = Select(browser.find_element(By.XPATH, value="//select[contains(@class,'seedid')]"))
    legume.select_by_value("64")
    sleep(randint(2,4))

    #efetivamente semeio
    browser.find_element(
        By.XPATH, value="//a[contains(@class,'plantall')]"
        ).click()
    sleep(randint(2,4))

    #confirmo, e espero xis segundos
    #Carrots 55s
    #Corn 1h40m -> 6000s
    #Hops 9m -> 540s
    #Leek 27m -> 1600s
    browser.find_element(
        By.XPATH, value="//div[contains(@class,'modal-button')]"
        ).click()
    print(sementeira)
    sleep(2)
    if sementeira < 9: # enqt tiver sementes, espero, e faço de novo.
        sleep(6000)
        sementeira += 1
    else:
        # Vou comprar sementes
        store_button = browser.find_element(By.XPATH, value="//a[contains(@href,'store')]")
        store_button.send_keys(Keys.DOWN)
        store_button.send_keys(Keys.DOWN)
        store_button.send_keys(Keys.DOWN)
        store_button.send_keys(Keys.DOWN)
        sleep(1)
        store_button.click()
        sleep(randint(5,7))

        #encho o cesto
        corn_button = browser.find_element(By.XPATH, value="//button[@class='maxqty' and @data-id='64']")
        corn_button.send_keys(Keys.DOWN)
        corn_button.send_keys(Keys.DOWN)
        corn_button.send_keys(Keys.DOWN)
        corn_button.send_keys(Keys.DOWN)
        sleep(1)
        corn_button.click()
        sleep(1)

        # compro
        browser.find_element(
            By.XPATH, value="//span[@class='price64']"
            ).click()
        sleep(1)

        #confirmo
        browser.find_element(
            By.XPATH, value="//div[contains(@class,'modal-button') and not(contains(@class,'color-red'))]"
            ).click()
        sleep(1)
        #volto para farm
        browser.find_element(
            By.XPATH, value="//*[contains(text(),'Go to Farm')]"
            ).click()
        sleep(1)

        sementeira = 1
        break

browser.close()