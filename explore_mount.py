from auth import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
from time import sleep
from random import randint

browser = webdriver.Firefox()
browser.implicitly_wait(1)

browser.get("https://farmrpg.com/#!/login.php")

sleep(2)

browser.find_element(
    By.CSS_SELECTOR, value="input[name='username']"
    ).send_keys(USERNAME)
browser.find_element(
    By.CSS_SELECTOR, value="input[name='password']"
    ).send_keys(PASSWORD)

sleep(1)

browser.find_element(
    By.CSS_SELECTOR, value="input[value='Login']"
    ).click()

sleep(randint(2,6))

minhocas = 200
# Função para ver a stamina que me resta.
def stamina_calc():
    staminas = browser.find_element(By.XPATH, value="//span[@id='stamina']").text
    return int(staminas)
    
while minhocas < 300:
    # Vou explorar
    browser.find_element(
        By.XPATH, value="//a[contains(@href,'explore.php')]"
        ).click()
    sleep(randint(4,7))

    # Escolho o sitio
    #el = browser.find_element(By.XPATH, value="//*[text()='Mount Banon']")
    #ActionChains(browser).move_to_element(el).click(el).perform()
    mount = browser.find_element(By.XPATH, value="//*[text()='Mount Banon']")
    browser.execute_script("arguments[0].scrollIntoView(true);", mount)
    #mount.send_keys(Keys.DOWN)
    #mount.send_keys(Keys.DOWN)
    #mount.send_keys(Keys.DOWN)
    #mount.send_keys(Keys.DOWN)
    print ('passa aki')
    sleep(1)
    mount.click()
    sleep(2)
        
    stamina = stamina_calc()

    while stamina > 24:
        print(stamina > 20)
        browser.find_element(
            By.XPATH, value="//div[@id='exploreconsole']"
            ).click()
        stamina = stamina_calc()
        print(stamina)
        sleep(randint(2,5))

    browser.find_element(
        By.XPATH, value="//a[@id='homebtn']"
        ).click()
    sleep(randint(3,6))

    # Vou pescar à Farm Pond, para restabelecer stamina
    browser.find_element(
        By.XPATH, value="//a[contains(@href,'fish.php')]"
        ).click()
    sleep(randint(4,7))

    browser.find_element(
        By.XPATH, value="//*[text()='Farm Pond']"
        ).click()
    sleep(2)

    contar_pescas = 0
    while contar_pescas < 25:
        #sleep(2)
        while True:
            try:
                browser.find_element(By.XPATH, value="//img[contains(@class,'catch')]").click()
                str_error = None
                print('clicou')
            except Exception as str_error:
                print(str_error)
                pass

            if not str_error:
                break

        #WebDriverWait(browser, timeout=20).until(
        #    EC.presence_of_element_located((By.XPATH, "//img[contains(@class,'catch')]"))
        #    )
        #sleep(0.25)
        
        #except
        
        sleep(1)
        
        browser.find_element(
            By.XPATH, value="//div[contains(@class,'fishcaught')]"
            ).click()
        minhocas -= 1
        contar_pescas += 1
        print(minhocas)
        print(contar_pescas)
        
    
    browser.find_element(
        By.XPATH, value="//a[@id='homebtn']"
        ).click()
    sleep(randint(3,6))

    
browser.close()

""" 
while True:
    #sementeira = 0
    #Apanho o que estiver plantado
    browser.find_element(
        By.XPATH, value="//a[contains(@class,'harvestall')]"
        ).click()
    sleep(randint(2,4))

    #seleciono o que quero semear
    #Carrots 20
    #Corn 64
    #Hops 47
    #Leek 51
    legume = Select(browser.find_element(By.XPATH, value="//select[contains(@class,'seedid')]"))
    legume.select_by_value("64")
    sleep(randint(2,4))

    #efetivamente semeio
    browser.find_element(
        By.XPATH, value="//a[contains(@class,'plantall')]"
        ).click()
    sleep(randint(2,4))

    #confirmo, e espero xis segundos
    #Carrots 55s
    #Corn 1h40m -> 6000s
    #Hops 9m -> 540s
    #Leek 27m -> 1600s
    browser.find_element(
        By.XPATH, value="//div[contains(@class,'modal-button')]"
        ).click()
    print(sementeira)
    sleep(2)
    if sementeira < 9: # enqt tiver sementes, espero, e faço de novo.
        sleep(6000)
        sementeira += 1
    else:
        # Vou comprar sementes
        store_button = browser.find_element(By.XPATH, value="//a[contains(@href,'store')]")
        store_button.send_keys(Keys.DOWN)
        store_button.send_keys(Keys.DOWN)
        store_button.send_keys(Keys.DOWN)
        store_button.send_keys(Keys.DOWN)
        sleep(1)
        store_button.click()
        sleep(randint(5,7))

        #encho o cesto
        corn_button = browser.find_element(By.XPATH, value="//button[@class='maxqty' and @data-id='64']")
        corn_button.send_keys(Keys.DOWN)
        corn_button.send_keys(Keys.DOWN)
        corn_button.send_keys(Keys.DOWN)
        corn_button.send_keys(Keys.DOWN)
        sleep(1)
        corn_button.click()
        sleep(1)

        # compro
        browser.find_element(
            By.XPATH, value="//span[@class='price64']"
            ).click()
        sleep(1)

        #confirmo
        browser.find_element(
            By.XPATH, value="//div[contains(@class,'modal-button') and not(contains(@class,'color-red'))]"
            ).click()
        sleep(1)
        #volto para farm
        browser.find_element(
            By.XPATH, value="//*[contains(text(),'Go to Farm')]"
            ).click()
        sleep(1)

        sementeira = 1
        break
"""