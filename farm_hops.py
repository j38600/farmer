from auth import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from time import sleep
from random import randint

browser = webdriver.Firefox()
browser.implicitly_wait(1)

browser.get("https://farmrpg.com/#!/login.php")

sleep(2)

browser.find_element(
    By.CSS_SELECTOR, value="input[name='username']"
    ).send_keys(USERNAME)
browser.find_element(
    By.CSS_SELECTOR, value="input[name='password']"
    ).send_keys(PASSWORD)

sleep(1)

browser.find_element(
    By.CSS_SELECTOR, value="input[value='Login']"
    ).click()

sleep(randint(2,6))

sementeira = 0
# Vou à quinta
browser.find_element(
    By.XPATH, value="//a[contains(@href,'xfarm')]"
    ).click()
sleep(randint(4,7))

while True:
    #sementeira = 0
    #Apanho o que estiver plantado
    browser.find_element(
        By.XPATH, value="//a[contains(@class,'harvestall')]"
        ).click()
    sleep(randint(2,4))

    #seleciono o que quero semear
    #Carrots 20
    #Corn 64
    #Hops 47
    #Leek 51
    legume = Select(browser.find_element(By.XPATH, value="//select[contains(@class,'seedid')]"))
    legume.select_by_value("47")
    sleep(randint(2,4))

    #efetivamente semeio
    browser.find_element(
        By.XPATH, value="//a[contains(@class,'plantall')]"
        ).click()
    sleep(randint(2,4))

    #confirmo, e espero xis segundos
    #Carrots 55s
    #Corn 1h40m -> 6000s
    #Hops 8m -> 480s
    #Leek 27m -> 1600s
    browser.find_element(
        By.XPATH, value="//div[contains(@class,'modal-button')]"
        ).click()
    print(sementeira)
    sleep(2)
    if sementeira < 10: # enqt tiver sementes, espero, e faço de novo.
        sleep(500)
        sementeira += 1
    else:
        # Vou comprar sementes
        store_button = browser.find_element(By.XPATH, value="//a[contains(@href,'store')]")
        store_button.send_keys(Keys.DOWN)
        store_button.send_keys(Keys.DOWN)
        store_button.send_keys(Keys.DOWN)
        store_button.send_keys(Keys.DOWN)
        sleep(1)
        print('merda')
        store_button.click()
        sleep(1)
        sleep(randint(4,7))

        #encho o cesto
        corn_button = browser.find_element(By.XPATH, value="//button[@class='maxqty' and @data-id='47']")
        corn_button.send_keys(Keys.DOWN)
        corn_button.send_keys(Keys.DOWN)
        corn_button.send_keys(Keys.DOWN)
        corn_button.send_keys(Keys.DOWN)
        sleep(1)
        corn_button.click()
        sleep(1)

        # compro
        browser.find_element(
            By.XPATH, value="//span[@class='price47']"
            ).click()
        sleep(1)

        #confirmo
        browser.find_element(
            By.XPATH, value="//div[contains(@class,'modal-button') and not(contains(@class,'color-red'))]"
            ).click()
        sleep(1)
        #fico na loja
        browser.find_element(
            By.XPATH, value="//span[contains(@class,'modal-button') and not(contains(@class,'bold'))]"
            ).click()
        sleep(1)

        # Vou até ao mercado
        browser.find_element(
            By.XPATH, value="//a[@id='homebtn']"
            ).click()
        sleep(randint(3,6))
        browser.find_element(
            By.XPATH, value="//a[contains(@href,'town')]"
            ).click()
        sleep(randint(2,4))
        browser.find_element(
            By.XPATH, value="//a[@href='market.php']"
            ).click()
        sleep(randint(3,6))
        #Vendo
        market_button = browser.find_element(By.XPATH, value="//button[@data-name='Hops']")
        market_button.send_keys(Keys.DOWN)
        market_button.send_keys(Keys.DOWN)
        market_button.send_keys(Keys.DOWN)
        market_button.send_keys(Keys.DOWN)
        sleep(1)
        market_button.click()
        
        sleep(randint(3,6))
        #confirmo
        browser.find_element(
            By.XPATH, value="//div[contains(@class,'modal-button') and not(contains(@class,'color-red'))]"
            ).click()
        sleep(1)
        browser.find_element(
            By.XPATH, value="//span[contains(@class,'modal-button')]"
            ).click()
        sleep(1)
        browser.find_element(
            By.XPATH, value="//a[@id='homebtn']"
            ).click()
        sleep(randint(3,6))

        #chego aki, o mais rápido após 28s
        browser.find_element(
            By.XPATH, value="//a[contains(@href,'xfarm')]"
            ).click()
        sleep(470)

        sementeira = 1

browser.close()